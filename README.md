# Pac Man

A C# remake of the popular arcade game, Pac Man. This reimagination consists of 3 game modes (Normal, Hardcore and Retro) and imported music from the Hotline Miami game.